(function(){
var app = angular.module('bookToRead', ['ngRoute','dataLibrary','dataTodo', 'usersData', 'bookToReadSelect']);


app.config(['$routeProvider', function($routeProvider) {
  $routeProvider.
    when('/login', {
      templateUrl:
        'partials/login.html',
      controller: 'LoginCtrl'
    }).
    when('/book-list',{
      templateUrl:
        'partials/book-list.html',
      controller: 'DataController as data'
    }).
    otherwise({
      redirectTo:'/login'
    });
}])

app.controller('LoginCtrl', ['$scope', '$rootScope', '$window', function($scope, $rootScope, $window, UsersList) {


  $scope.logInUser = function(user) {
    if (firebase.auth().currentUser) {
      firebase.auth().signOut();
    }
    console.log(user.email);
    firebase.auth().signInWithEmailAndPassword(user.email, user.password).then(function() {
      $window.location.href = $window.location.pathname + '#/book-list';
    }).catch(function(error) {
      // Handle Errors here.
      var errorCode = error.code;
      console.log(errorCode);
      var errorMessage = error.message;
      $('.error-logIn').text(errorMessage);
    })
  };



}]);


app.controller('DataController', ['$rootScope','$scope', '$timeout', '$window',  function($rootScope, $scope, $timeout, $window) {
var bookList = this;
  var database = firebase.database();
  firebase.database().ref('library/').on('value', function(snapshot) {
    $timeout(function() {
      var snapshotVal = snapshot.val();
      var currentArray = [];
      $.each(snapshotVal, function(index, value){
        currentArray.push(value);
      });
      $scope.books = currentArray;
      $rootScope.booksLibrary = snapshotVal;
    })
  });

  bookList.logOutUser = function() {
    console.log("logOutUser");
    firebase.auth().signOut();
    $window.location.href = $window.location.pathname + '#/login';
  }

	bookList.selectTab = function(setTab) {
		this.tab = setTab;
	};

	bookList.isSelected = function(checkTab) {
		return this.tab === checkTab;
	};

	bookList.infoBook = function(book) {
		bookList.currentBook = book;
		bookList.selectTab(true);
	};

  bookList.ifReadBook = function(book) {
    return book.done ? "read" : "unread";
  }

	$scope.orderProp = 'rating_stars';

}]);


app.controller('TodoListController',['$rootScope','$scope', 'toDoLibrary', '$timeout', function($rootScope, $scope, toDoLibrary, $timeout) {
	var todoList = this;

  var database = firebase.database();
  database.ref('toDoLibrary/').on('value', function(snapshot) {
    $timeout(function() {
      var snapshotVal = snapshot.val();
      var currentArray = [];
      $.each(snapshotVal, function(index, value){
        currentArray.push(value);
      });
      $scope.todos = currentArray;
      $scope.toDoLibrary = snapshotVal;
    })
  });

  todoList.read = "all";
  $scope.tab = false;
  todoList.todoItem = null;

  $scope.selectTab = function(setTab) {
    $scope.tab = setTab;
  };

  $scope.isSelected = function(checkTab) {
    return $scope.tab === checkTab;
  };

  todoList.selectTodoItem = function(setTab) {
    this.todoItem = setTab;
  };

  todoList.isSelectedTodoItem = function(checkTab) {
    return this.todoItem === checkTab;
  };

  todoList.todoInfo = function(todo) {
    todo.done = false
    $scope.currentTodoBook = todo;
    $scope.selectTab(true);
  };

  todoList.saveBookToLibrary = function() {
    $rootScope.booksLibrary.push($scope.currentTodoBook);
    todoList.selectTab(false);
  }

  todoList.readBookDone = function(todo) {
    var currentIndex;
    $.each($scope.toDoLibrary, function(index, value){
      if(value.name === todo.name) {
        if(value.done === true) {
          firebase.database().ref('toDoLibrary/' + index).update({
            "done" : false
          });
        } else {
          firebase.database().ref('toDoLibrary/' + index).update({
            "done" : true
          });
        }
      }
    });
    $.each($rootScope.booksLibrary, function(index, value){
      if(value.name === todo.name) {
        if(value.done === true) {
          firebase.database().ref('library/' + index).update({
            "done" : false
          });
        } else {
          firebase.database().ref('library/' + index).update({
            "done" : true
          });
        }
      }
    });
  }

  todoList.setRead = function(status) {
    todoList.read = status;
  };

  todoList.addTodo = function() {
    var newToDo = {
      name:todoList.todoText,
      done:false,
      viewAll:"all",
      save_library: "no save"
    };
    var currentToDo = angular.fromJson(angular.toJson(newToDo));
    firebase.database().ref('toDoLibrary').push(currentToDo);
    todoList.todoText = '';
  };

  todoList.remaining = function() {
    var count = 0;
    angular.forEach($scope.todos, function(todo) {
      count += todo.done ? 0 : 1;
    });
    return count;
  };

  todoList.archive = function() {
    var oldTodos = $scope.todos;
    firebase.database().ref('toDoLibrary/').remove();
    angular.forEach(oldTodos, function(todo) {
      var currentToDo = angular.fromJson(angular.toJson(todo));
      if (!todo.done) firebase.database().ref('toDoLibrary/').push(currentToDo);
    });
  };

}]);

app.controller('formAddCntrl', ['$rootScope', '$scope', function($rootScope, $scope) {

  $scope.update = function(currentTodoBook) {
    if (currentTodoBook.name && currentTodoBook.author && currentTodoBook.rating_stars && currentTodoBook.genre) {
      if (!currentTodoBook.imageUrl) {
        currentTodoBook.imageUrl = "img/books/Title_books.jpg";
      }
      currentTodoBook.save_library = "save";
      var current = angular.fromJson(angular.toJson(currentTodoBook));
      firebase.database().ref('library/').push(current);
      $.each($scope.toDoLibrary, function(index, value){
        if(value.name === currentTodoBook.name) {
          firebase.database().ref('toDoLibrary/' + index).update({
            "save_library" : "save"
          });
        }
      });
      $scope.selectTab(false);
    } else {
      console.log("Not all fields the form are required and valid!!!");
    }
  };
  }]);

})();
