(function(){

var app = angular.module( 'dataLibrary', []);

app.factory('libraryBook', function(){
    return{
      library:
       [
          {
              "rating_stars": 10,
              "done":true,
              "genre": "novel",
              "sortAll": "all",
              "imageUrl": "img/books/Title_books.jpg",
              "name": "Война и мир",
              "author": "Толстой Лев",
              "save_library": true
          },
          {
              "rating_stars": 9,
              "done":false,
              "genre": "horor",
              "sortAll": "all",
              "imageUrl": "img/books/Title_books.jpg",
              "name": "Марсианин",
              "author": "Вейер Энди",
              "save_library": true
          },
          {
              "rating_stars": 8,
              "done":false,
              "genre": "novel",
              "sortAll": "all",
              "imageUrl": "img/books/Title_books.jpg",
              "name": "Анна Каренина",
              "author": "Толстой Лев",
              "save_library": true
          },
          {
              "rating_stars": 7,
              "done":false,
              "genre": "detective",
              "sortAll": "all",
              "imageUrl": "img/books/Title_books.jpg",
              "name": "Клуб оптимистов",
              "author": "Генассия Жан-Мишель",
              "save_library": true
          },
          {
              "rating_stars": 6,
              "done":true,
              "genre": "novel",
              "sortAll": "all",
              "imageUrl": "img/books/Title_books.jpg",
              "name": "Люди в голом",
              "author": "Аствацатуров Андрей",
              "save_library": true
          },
          {
              "rating_stars": 5,
              "done":true,
              "genre": "novel",
              "sortAll": "all",
              "imageUrl": "img/books/Title_books.jpg",
              "name": "Шантарам",
              "author": "Робертс Грегори Дэвид",
              "save_library": true
          },
          {
              "rating_stars": 4,
              "done":true,
              "genre": "horor",
              "sortAll": "all",
              "imageUrl": "img/books/Title_books.jpg",
              "name": "Шан-Хан",
              "author": "Робертс Грегори Дэвид",
              "save_library": true
          },
          {
              "rating_stars": 3,
              "done":false,
              "genre": "detective",
              "sortAll": "all",
              "imageUrl": "img/books/Title_books.jpg",
              "name": "Я исповедуюсь",
              "author": " Кабре Жауме",
              "save_library": true
          },
          {
              "rating_stars": 2,
              "done":true,
              "genre": "novel",
              "sortAll": "all",
              "imageUrl": "img/books/Title_books.jpg",
              "name": "До встречи с тобой",
              "author": "Мойес Джоджо",
              "save_library": true
          },
          {
              "rating_stars": 1,
              "done":true,
              "genre": "detective",
              "sortAll": "all",
              "imageUrl": "img/books/Title_books.jpg",
              "name": "Лавр",
              "author": "Водолазкин Евгений",
              "save_library": true
          }
        ]

    };
});

})();
