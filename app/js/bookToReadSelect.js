(function(){
  var app = angular.module('bookToReadSelect', []);

app.directive('genreSelect', function() {
  return {
    restrict: "E",
    templateUrl: "partials/genre-select-template.html",
    controller: function () {
      this.genreType = {
        all: "all",
        novel: "novel",
        detective: "detective",
        horor: "horor"
      };
    },
    controllerAs: "genreFilter"
  };
});

app.directive('sortSelect', function() {
  return {
    restrict: "E",
    templateUrl: "partials/sort-select-template.html",
    controller: function () {
      this.sortFilter = [
        {
          name: "Alphabetical",
          value: "name"
        },
        {
          name: "Rating Stars",
          value: "-rating_stars"
        }
      ];
    },
    controllerAs: "filterSortCntrl"
  };
});

})();
