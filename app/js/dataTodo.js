(function(){

var app = angular.module( 'dataTodo', []);

app.factory('toDoLibrary', function(){
    return{
      library:
        [
          {
              "name":"Шантарам",
              "done":true,
              "viewAll":"all",
              "save_library": "save"
          },
          {
              "name":"Война и мир",
              "done":true,
              "viewAll":"all",
              "save_library": "save"
          },
          {
              "name":"Евгений Онегин",
              "done":false,
              "viewAll":"all",
              "save_library": "no save"
          },
          {
              "name":"Шан-Хан",
              "done":true,
              "viewAll":"all",
              "save_library": "save"
          },
          {
              "name":"Мертвые души",
              "done":false,
              "viewAll":"all",
              "save_library": "no save"
          },
          {
              "name":"Люди в голом",
              "done":true,
              "viewAll":"all",
              "save_library": "save"
          },
          {
              "name":"Лавр",
              "done":true,
              "viewAll":"all",
              "save_library": "save"
          },
          {
              "name":"Я исповедуюсь",
              "done":true,
              "viewAll":"all",
              "save_library": "save"
          },
          {
              "name":"Мастер и Маргарита",
              "done":false,
              "viewAll":"all",
              "save_library": "no save"
          },
          {
              "name":"Гарри Поттер и тайная комната",
              "done":false,
              "viewAll":"all",
              "save_library": "no save"
          }
        ]
    };
});

})();
